<?php namespace Eae\Sentry\Raven;

use Monolog\Handler\RavenHandler;
use Monolog\Logger;

class Handler extends RavenHandler
{
    public function __construct($ravenClient, $level = Logger::ERROR, $bubble = true)
    {

        if ( is_array($ravenClient) && array_key_exists('instance', $ravenClient) )
        {
            $ravenClient = new $ravenClient['instance'];
        }

        parent::__construct($ravenClient, $level, $bubble);
    }

    protected function write(array $record){
        if (isset($record['context']['is_exception']) && $record['context']['is_exception']) {
           unset($record['context']['is_exception']);
           parent::write($record);
       }
    }
}