<?php namespace Eae\Sentry\Raven;

use Raven_Client;

/**
 * Raven Client - Uses Raven DNS and options from env.php
 *
 * @package        Eae\Sentry
 * @author         Eae
 * @copyright      Copyright (c) 2017, Eae. All rights reserved
 */
class Client extends Raven_Client
{
    public function __construct()
    {
        $ravenDNS = null;
        $options  = [ ];

        //TODO to cli
        if ( php_sapi_name() !== 'cli' )
        {
            $root = $_SERVER[ 'DOCUMENT_ROOT' ];

            if ( is_file($envFile = $root . '/app/etc/env.php') )
            {
                $env = include $envFile;
            }
            else
            {
                $env = include $root . '/../app/etc/env.php';
            }

            $isDeveloper = array_key_exists('MAGE_MODE', $env) && $env[ 'MAGE_MODE' ] === 'developer';

            // Only log to Sentry if the use is not in development mode
            if ( !$isDeveloper )
            {
                if(array_key_exists('sentry', $env)){
                    $sentryCfg = $env[ 'sentry' ];
                    $ravenDNS = array_key_exists('raven_dns', $sentryCfg) ? $sentryCfg[ 'raven_dns' ] : null;
                    $options = array_key_exists('options', $sentryCfg) ? $sentryCfg[ 'options' ] : [ ];
                }

            }
        }

        parent::__construct($ravenDNS, $options);
    }
}