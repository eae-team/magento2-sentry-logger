# Magento 2.0 Sentry Logger

This extension will add the ability to log to [Sentry](https://github.com/getsentry/). Default for the minimal logging level is DEBUG, this is set in the extensions di.xml.

## Installation with composer
* Include the repository: `composer require EastAtlantic/magento2-sentry-logger`
* Enable the extension: `php bin/magento --clear-static-content module:enable Eae_Sentry`
* Upgrade db scheme: `php bin/magento setup:upgrade`
* Clear cache

## Installation without composer
* Download zip file of this extension
* Place all the files of the extension in your Magento 2 installation in the folder `app/code/Eae/Sentry`
* Enable the extension: `php bin/magento --clear-static-content module:enable Eae_Sentry`
* Upgrade db scheme: `php bin/magento setup:upgrade`
* Clear cache

## Configuration
* Add the variable 'sentry' to your app/etc/env.php file. Example:

```
'sentry' => [
    'raven_dns' => 'https://****@sentry.domain.com/8',
    'options' => []
]
```

* To make it work has async change the options to:

```
'options' => [
    'curl_method' => 'exec'
]

```

But you have to check if curl is available in your server first.

* To check further option please check https://docs.sentry.io/clients/php/config/
